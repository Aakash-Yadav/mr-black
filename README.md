
### Flask anime downloading web application

![alt text](https://xp.io/storage/2wBnycmQ.png)

_this web-app is all about web scraping models and anime_

### requirements module 
- ##### pip install Flask 
- ##### pip install requests 

### What is Flask Python

Flask is a web framework, it’s a Python module that lets you develop web applications easily. It’s has a small and easy-to-extend core: it’s a microframework that doesn’t include an ORM (Object Relational Manager) or such features.

### What is Flask?

Flask is a web application framework written in Python. It was developed by Armin Ronacher, who led a team of international Python enthusiasts called Poocco. Flask is based on the Werkzeg WSGI toolkit and the Jinja2 template engine.Both are Pocco projects.

### Home page of web-app
![alt text](https://xp.io/storage/jmjp9BD.png)
###### anime image and anime description
![alt text](https://xp.io/storage/jmrz7Qn.png)

### Search page
![alt text](https://xp.io/storage/2wC15rjI.png)
#### Autocomplete Feature
![alt text](https://xp.io/storage/2wC8IEuG.png)
#### Info page :-
![alt text](https://xp.io/storage/jmvtcDN.png)

####  Select episodes for watching 
just put number of episode
![alt text](https://xp.io/storage/jmCazEP.png)

#### Server Page :)
![alt text](https://xp.io/storage/165NjUxy.png)

#### Demo for server
![alt text](https://xp.io/storage/jmOi0D0.png)

#### Webapp Link 
### https://asteroidfire.pythonanywhere.com/