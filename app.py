from crypt import methods
from random import randint
from flask import Flask ,render_template,session,redirect,url_for,request,flash
from os import urandom
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__);

app.config["SECRET_KEY"] = f"{urandom(250)}"
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///ANIME_DB.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 0;


db = SQLAlchemy(app)

class BASIC_DATA_OF_ANIME(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(350), nullable=False)
    image = db.Column(db.String(350))
    description = db.Column(db.Text)
    total_ep = db.Column(db.Integer, nullable=False)
    link = db.Column(db.String(350))

'ROUTES'
############################################################
############################################################
############################################################
############################################################

@app.route("/")
@app.route("/home")

def home():
    out = dict();
    for i in range(100):
        anime_data = BASIC_DATA_OF_ANIME.query.filter_by(id=randint(0,11000)).first()
        out.update({f"{i}":{"name":anime_data.name,"image":anime_data.image,"Description":anime_data.description}})
    return render_template('home.html',data=out);

@app.route('/search',methods=['GET','POST'])
def Search():
    if request.method == 'POST':
        session['Anime_name'] = request.form.get('somthing')
        return redirect(url_for('Information'))
    return render_template('index.html')

@app.route('/search_define/<n>',methods=['GET','POST'])
def Search_By_name(n):
    print(n)
    session['Anime_name'] = n
    return redirect(url_for('Information'))


@app.route('/information',methods=['GET','POST'])
def Information():
    try:
        val =   BASIC_DATA_OF_ANIME.query.filter_by(name=session['Anime_name']).first()

        new_data= {
        'image':val.image,
        'summary':val.description,
        'total_ep':val.total_ep,
        'name_of_anime':val.name,
    }
        session['total_ep_'] = new_data['total_ep']
        session['name_watching_info'] = new_data['name_of_anime']
    except:
        return redirect(url_for('Search'))
    basic_data = new_data

    return render_template('download.html',img=basic_data['image'],name=basic_data['name_of_anime'].upper() ,pera=basic_data['summary'],Tep=basic_data['total_ep']
    )

@app.route('/watch_episode',methods=['GET','POST'])
def watch_episode_anime():
    try:
        get_anime_data = BASIC_DATA_OF_ANIME.query.filter_by(name=session['name_watching_info']).first()

        values = {
            'total_ep':(get_anime_data.total_ep),
            'server_links':get_anime_data.link
        }
    except:
        return redirect('Information')
    if request.method == 'POST':
        epsode = request.form.get('user')
        session['data_of_epsode_of_uyser_data'] = epsode
        try:
            session['data_of_epsode_of_uyser_data'] = int(session['data_of_epsode_of_uyser_data'])
            if session['data_of_epsode_of_uyser_data']>values["total_ep"] or session['data_of_epsode_of_uyser_data']<=0:
                flash(f'sorry link not found for episode {session["ep"]}',category='error')
            else:
                session['data_of_live_server'] = ''.join(values["server_links"].split("episode")[0])+f"episode-{session['data_of_epsode_of_uyser_data']}.html"
                return redirect(url_for('server_links'))
        except:
            flash(f'not able to find link for {epsode}',category='error')
            return redirect(url_for('watch_episode_anime'))

    return render_template('form_for_watch_episode.html',num=values['total_ep'])

@app.route('/server')
def server_links():
    try:
        links = session['data_of_live_server']
        return render_template('server.html',data=links[:-5])
    except:
        return redirect(url_for('Information'))

@app.route("/update_server_of__the_anime_databse",methods=["POST","GET"])
def update_server_of__the_anime_databse():
    if request.method == "POST":
        all_father = request.form.get("name_of_anime")
        new_ep = all_father.split("$$#$$")[-1]
        name = all_father.split("$$#$$")[0]
        anime_ep_number = int(new_ep[:-5].split("-episode-")[-1])
        # print(name,new_ep)
        up_date= BASIC_DATA_OF_ANIME.query.filter_by(name=name).first()
        up_date.link = new_ep
        up_date.total_ep = anime_ep_number
        db.session.commit()
        return "TRUE"
    else:
        return redirect(url_for("home"))


@app.route("/update_server_of__the__new__anime_databse",methods=["POST","GET"])
def update_server_of__the__new__anime_databse():
    if request.method == "POST":
        name = request.form.get("name")
        anime_dec = request.form.get("Description")
        anime_ep_number = request.form.get("Total_ep")
        image_link = request.form.get("image")
        new_ep = request.form.get("episode_links")
        up_date= BASIC_DATA_OF_ANIME(name=name,image=image_link,description=anime_dec,
                                     total_ep=anime_ep_number,link=new_ep)
        db.session.add(up_date)
        db.session.commit()
        return "TRUE"
    else:
        return redirect(url_for("home"))

if __name__ == "__main__":
    app.run(debug=0)
    
